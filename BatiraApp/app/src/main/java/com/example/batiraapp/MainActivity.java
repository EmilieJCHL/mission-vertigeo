package com.example.batiraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    // I - Instanciation des objets Java représentant les composants graphiques
    home = findViewById(R.id.home);


    // II - Ajout des écouteurs d'événements aux composants graphiques représentés par des objets Java
    home.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            MainActivity.this.openHomeActivity();
        }
    });

    }

    /**
     * Ouverture de l'activité Home pour tester
     */
    public void openHomeActivity(){
        // Création de l'intention
        Intent intent = new Intent(this, HomeActivity.class);
        // Lancement de l'activité
        startActivity(intent);
    }
}