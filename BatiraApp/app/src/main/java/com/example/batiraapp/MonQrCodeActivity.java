package com.example.batiraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;

public class MonQrCodeActivity extends AppCompatActivity {

    Button genererQrCodeB;
    Button menuB;
    Button retourB;


    ImageView qr_code;
    public static Bitmap bmp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        // 0 - Définition de la couche à utiliser
        setContentView(R.layout.activity_mon_qr_code);

        // I - Instanciation des objets Java représentant les composants graphiques
        genererQrCodeB = findViewById(R.id.genererQrCodeB);
        menuB = findViewById(R.id.menuB);
        retourB = findViewById(R.id.retourB);
        qr_code = findViewById(R.id.qr_code);

        Intent intent = getIntent();

        if (intent.hasExtra("qrcode")){ // vérifie qu'une valeur est associée à la clé “edittext”
            System.out.println("here");
            byte[] byteArray = getIntent().getByteArrayExtra("qrcode");
            bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            qr_code.setImageBitmap(bmp);
        }

        if ( null != bmp ){
            System.out.println("laaaaaaaaaaaaaa");
            qr_code.setImageBitmap(bmp);
        }




        // II - Ajout des écouteurs d'événements aux composants graphiques représentés par des objets Java
        genererQrCodeB.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                MonQrCodeActivity.this.openGenerateQrCodeActivity();
            }
        });


        retourB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MonQrCodeActivity.this.openQrCodeNotesActivity();
            }
        });
    }


    /**
     * Ouverture de l'activité Generate Qr Code
     */
    public void openGenerateQrCodeActivity(){
        // Création de l'intention
        Intent intent = new Intent(this,GenerateQrCodeActivity.class);
        // Lancement de l'activité
        startActivity(intent);
    }

    /**
     * Ouverture de l'activité Qr Code Notes
     */
    public void openQrCodeNotesActivity(){
        // Création de l'intention
        Intent intent = new Intent(this, QrCodeNotesActivity.class);
        // Lancement de l'activité
        startActivity(intent);
    }



}