package com.example.batiraapp;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import com.google.zxing.WriterException;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import java.io.ByteArrayOutputStream;
import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;


public class GenerateQrCodeActivity extends AppCompatActivity {

    Button retourB;
    Button menuB;

    String TAG = "GenerateQRCode";

    EditText textnom;
    EditText textprenom;
    EditText textpromotion;

    ImageView qrImage;
    Button start, saveB;
    String inputValue;
    Bitmap bitmap;
    Bitmap image;


    QRGEncoder qrgEncoder = new QRGEncoder(inputValue, null, QRGContents.Type.TEXT, 3);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //0 - Définition de la couche à utiliser
        setContentView(R.layout.activity_generate_qr_code);

        // I - Instanciation des objets Java représentant les composants graphiques
        retourB = findViewById(R.id.retourB);
        menuB = findViewById(R.id.menuB);

        qrImage = (ImageView) findViewById(R.id.qr_image);

        textnom = (EditText) findViewById(R.id.textnom);
        textprenom = (EditText) findViewById(R.id.textprenom);
        textpromotion = (EditText) findViewById(R.id.textpromotion);
        start = (Button) findViewById(R.id.start);
        saveB = (Button) findViewById(R.id.saveB);




        // II - Ajout des écouteurs d'événements aux composants graphiques représentés par des objets Java
        menuB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // à remplir quand le menu qui se déroule sur le côté sera codé
            }
        });
        retourB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GenerateQrCodeActivity.this.openMonQrCodeActivity();
            }
        });

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { //Quand l'utilisateur clique sur le bouton start

                //Récupération des valeurs saisies par l'utilisateur
                inputValue = textnom.getText().toString().trim() + ' ' + textprenom.getText().toString().trim() + ' ' +
                        textpromotion.getText().toString().trim() ;

                //Création d'un QR code
                if (inputValue.length() > 0) {
                    WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
                    Display display = manager.getDefaultDisplay();
                    Point point = new Point();
                    display.getSize(point);
                    int width = point.x;
                    int height = point.y;
                    int smallerDimension = Math.min(width, height);
                    smallerDimension = smallerDimension * 3 / 4;

                    qrgEncoder = new QRGEncoder(
                            inputValue, null,
                            QRGContents.Type.TEXT,
                            smallerDimension);
                    try { //Affichage de l'image du QR code
                        bitmap = qrgEncoder.encodeAsBitmap();
                        qrImage.setImageBitmap(bitmap);
                        image =((BitmapDrawable)qrImage.getDrawable()).getBitmap();

                    } catch (WriterException e) {
                        Log.v(TAG, e.toString());
                    }
                } else {
                    textnom.setError("Required");
                }
            }

        });

        saveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GenerateQrCodeActivity.this.envoieQrImageActivity();

            }
        });



    }

    /**
     * Ouverture de l'activité Home (Accueil) quand l'utilisateur clique sur le bouton retour
     */
    public void openMonQrCodeActivity(){
        // Création de l'intent
        Intent intent = new Intent(this, MonQrCodeActivity.class);
        // Lancement de l'activité
        startActivity(intent);
    }
    /**
     * Ouverture de l'activité MonQrCode quand l'utilisateur clique sur le bouton save (enregistrer)
     * et affichage du QR code sur la vue de cette activité
     */

    public void envoieQrImageActivity(){

        //Conversion de l'image du QR code en bytea, format dans lequel le QR code est envoyé à l'activité MonQrCodeActivity
        ByteArrayOutputStream bStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, bStream);
        byte[] bytea = bStream.toByteArray();

        Intent intent = new Intent(this, MonQrCodeActivity.class);
        intent.putExtra("qrcode", bytea);
        startActivity(intent);
    }


}