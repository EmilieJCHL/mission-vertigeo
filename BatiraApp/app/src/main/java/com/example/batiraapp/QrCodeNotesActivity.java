package com.example.batiraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class QrCodeNotesActivity extends AppCompatActivity {

    Button scannerQrCodeB;
    Button notesB;
    Button monQrCodeB;
    Button menuB;
    Button retourB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 0 - Définition de la couche à utiliser
        setContentView(R.layout.activity_qr_code_notes);

        // I - Instanciation des objets Java représentant les composants graphiques
        scannerQrCodeB = findViewById(R.id.scannerQrCodeB);
        notesB = findViewById(R.id.notesB);
        monQrCodeB = findViewById(R.id.monQrCodeB);
        menuB = findViewById(R.id.menuB);
        retourB = findViewById(R.id.retourB);


        // II - Ajout des écouteurs d'événements aux composants graphiques représentés par des objets Java
        scannerQrCodeB.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                QrCodeNotesActivity.this.openScanQrCodeActivity();
            }
        });

        notesB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QrCodeNotesActivity.this.openNoteActivity();
            }
        });
        monQrCodeB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QrCodeNotesActivity.this.openMonQrCodeActivity();
            }
        });
        retourB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QrCodeNotesActivity.this.openHomeActivity();
            }
        });
    }


    /**
     * Ouverture de l'activité plan
     */
    public void openScanQrCodeActivity(){
        // Création de l'intention
        Intent intent = new Intent(this, ScanQrCodeActivity.class);
        // Lancement de l'activité
        startActivity(intent);
    }

    /**
     * Ouverture de l'activité agenda
     */
    public void openNoteActivity(){
        // Création de l'intention
        Intent intent = new Intent(this, NoteActivity.class);
        // Lancement de l'activité
        startActivity(intent);
    }

    /**
     * Ouverture de l'activité exposants
     */
    public void openMonQrCodeActivity(){
        // Création de l'intention
        Intent intent = new Intent(this, MonQrCodeActivity.class);
        // Lancement de l'activité
        startActivity(intent);
    }
    /**
     * Ouverture de l'activité qr code
     */
    public void openHomeActivity(){
        // Création de l'intention
        Intent intent = new Intent(this, HomeActivity.class);
        // Lancement de l'activité
        startActivity(intent);
    }

}