package com.example.batiraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ProgrammeActivity extends AppCompatActivity {

    Button retourB;
    Button menuB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 0 - Définition de la couche à utiliser
        setContentView(R.layout.activity_programme);

        // I - Instanciation des objets Java représentant les composants graphiques
        retourB = findViewById(R.id.retourB);
        menuB = findViewById(R.id.menuB);

        // II - Ajout des écouteurs d'événements aux composants graphiques représentés par des objets Java
        menuB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // A REMPLIR quand le menu qui se déroule sur le côté sera codé
            }
        });
        retourB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProgrammeActivity.this.openAgendaActivity();
            }
        });
    }
        /**
         * Ouverture de l'activité Agenda quand l'utilisateur clique sur le bouton retour
         */
        public void openAgendaActivity(){
            // Création de l'intention
            Intent intent = new Intent(this, AgendaActivity.class);
            // Lancement de l'activité
            startActivity(intent);
        }
    }
