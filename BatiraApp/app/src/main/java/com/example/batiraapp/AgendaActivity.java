package com.example.batiraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AgendaActivity extends AppCompatActivity {

    Button monAgendaB;
    Button programmeB;
    Button ateliersB;
    Button retourB;
    Button menuB;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 0 - Définition de la couche à utiliser
        setContentView(R.layout.activity_agenda);

        // I - Instanciation des objets Java représentant les composants graphiques
        monAgendaB = findViewById(R.id.monAgendaB);
        programmeB = findViewById(R.id.programmeB);
        ateliersB = findViewById(R.id.ateliersB);
        retourB = findViewById(R.id.retourB);
        menuB = findViewById(R.id.menuB);


        // II - Ajout des écouteurs d'événements aux composants graphiques représentés par des objets Java
        monAgendaB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AgendaActivity.this.openMonAgendaActivity();
            }
        });
        programmeB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AgendaActivity.this.openProgrammeActivity();
                System.out.println("cette");
            }
        });
        ateliersB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("cette");
                AgendaActivity.this.openAteliersActivity();
            }
        });
        menuB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // A REMPLIR quand le menu qui se déroule sur le côté sera codé
            }
        });
        retourB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AgendaActivity.this.openHomeActivity();
            }
        });
    }


    /**
     * Ouverture de l'activité Mon Agenda
     */
    public void openMonAgendaActivity(){
        // Création de l'intention
        Intent intent = new Intent(this, MonAgendaActivity.class);
        // Lancement de l'activité
        startActivity(intent);
    }

    /**
     * Ouverture de l'activité Programme
     */
    public void openProgrammeActivity(){
        // Création de l'intention
        Intent intent= new Intent(this, ProgrammeActivity.class);
        // Lancement de l'activité
        startActivity(intent);
    }

    /**
     * Ouverture de l'activité ateliers
     */
    public void openAteliersActivity(){
        // Création de l'intention
        Intent intent = new Intent(this, AteliersActivity.class);
        // Lancement de l'activité
        startActivity(intent);
    }

    /**
     * Ouverture de l'activité Home (Accueil) quand l'utilisateur clique sur le bouton retour
     */
    public void openHomeActivity(){
        // Création de l'intention
        Intent intentHome = new Intent(this, HomeActivity.class);
        // Lancement de l'activité
        startActivity(intentHome);
    }
}



