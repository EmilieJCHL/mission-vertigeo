package com.example.batiraapp;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {

    Button planB;
    Button exposantB;
    Button agendaB;
    Button qrB;
    Button compteB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 0 - Définition de la couche à utiliser
        setContentView(R.layout.activity_home);

        // I - Instanciation des objets Java représentant les composants graphiques
        planB = findViewById(R.id.planB);
        exposantB = findViewById(R.id.exposantB);
        agendaB = findViewById(R.id.agendaB);
        qrB = findViewById(R.id.qrB);
        compteB = findViewById(R.id.compteB);


        // II - Ajout des écouteurs d'événements aux composants graphiques représentés par des objets Java
        planB.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                HomeActivity.this.openMapsActivity();
            }
        });
        agendaB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.this.openAgendaActivity();
            }
        });
        exposantB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.this.openExposantsActivity();
            }
        });
        qrB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.this.openQrCodeNotesActivity();
            }
        });
    }
    

    /**
     * Ouverture de l'activité plan
     */
    public void openMapsActivity(){
        // Création de l'intention
        Intent intentPlan = new Intent(this, MapsActivity.class);
        // Lancement de l'activité
        startActivity(intentPlan);
    }

    /**
     * Ouverture de l'activité agenda
     */
    public void openAgendaActivity(){
        // Création de l'intention
        Intent intentAgenda = new Intent(this, AgendaActivity.class);
        // Lancement de l'activité
        startActivity(intentAgenda);
    }

    /**
     * Ouverture de l'activité exposants
     */
    public void openExposantsActivity(){
        // Création de l'intention
        Intent intentExposants = new Intent(this, ExposantsActivity.class);
        // Lancement de l'activité
        startActivity(intentExposants);
    }
    /**
     * Ouverture de l'activité qr code
     */
    public void openQrCodeNotesActivity(){
        // Création de l'intention
        Intent intentQrCode = new Intent(this, QrCodeNotesActivity.class);
        // Lancement de l'activité
        startActivity(intentQrCode);
    }


}