package com.example.batiraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ScanQrCodeActivity extends AppCompatActivity {
    Button retourB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr_code);

        // I - Instanciation des objets Java représentant les composants graphiques
        retourB = findViewById(R.id.retourB);
        // II - Ajout des écouteurs d'événements aux composants graphiques représentés par des objets Java
        retourB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScanQrCodeActivity.this.openQrCodeNotesActivity();
            }
        });
    }

    /**
     * Ouverture de l'activité Qr Code Notes
     */
    public void openQrCodeNotesActivity(){
        // Création de l'intention
        Intent intent = new Intent(this, QrCodeNotesActivity.class);
        // Lancement de l'activité
        startActivity(intent);
    }
}